# Codeversionierung - Amar Memagic
## Aufgabe 1
1. Git lokal installiert
2. Repo über GitLab aufgesetzt
3. Git konfigurieren und Repo klonen
```console
git config --global user.name "amemagic"
git config --global user.email "amemagic@tsn.at"
git clone https://gitlab.com/amemagic/fse_codeversionierungsolo.git
```
4. README-File im Repo geändert
5. Status anzeigen und Änderungen durch git pushen
```console
git status (*Veränderte Dateien rot angezeigt*)
git add README.md
git status (*Datei staged*)
git commit -m "README.md aktualisiert"
git push origin main
```

### Notizen
- Erfahrung mit git über IDEs bereits durch die letzten Semester
- Anstatt einezlne Dateien mit git add x können mit git commit -a -m alle Dateien commited werden

## Aufgabe 2
Github-Flow Branching ermöglicht es Projekte in "Zweige" aufzuteilen. Dadurch können Entwickler unabhängig voneinander an Featuren oder Softwarepaketen arbeiten ohne einander zu verhindern. Während der Entwicklung können Änderungen auf den jeweiligen Zweigen vorgenommen oder zurückgerollt werden ohne den Standardbranch zu beeinflussen. Änderungen werden erst im Standardbranch übernommen wenn ein Zweig gemergt wird.

### Wichtige Konzepte
- Branching: Das Erstellen eines neuen Zweiges
- Pull-Requests: Anfragen zum Mergen
- Rebasing: Holt Änderungen aus der Standardbranch damit der Entwickler selbst Merge-Konflikte verhindern kann

### Befehle
```console
git clone
git - b checkout
git checkout
git pull
git commit -m "Commit-Message"
git push
git merge
git branch 
```

### Notizen
- Änderungen in vereinzelten Commits ermöglichen es auch im Nachhinein eine Änderung zurückzurollen aber eine andere beizubehalten.
Interessante Ressource dazu: https://docs.github.com/de/get-started/quickstart/github-flow

## Aufgabe 3
https://gitlab.com/amemagic/fse_codeversionierungteam

## Aufgabe 4
*Alternativen zu Github Flow*

### Git Flow
Git Flow ist die wohl bekannteste Branching-Strategie und basiert hauptsächlich auf zwei durchgehenden Branches.

- ```master``` - enthält ```production```-Code. Jeder ```development```-Code wird schlussendlich in diese Branch zusammengeführt.
- ```develop``` - enthält ```pre-production```-Code. Wenn Feature fertiggestellt werden werden sie in diese Branch zusammengeführt.

Zudem werden in der Entwicklungsphase weitere Support-Branches verwendet. Beispiele dafür:
- ```feature-*``` - Feature-Branches.
- ```hotfix-*``` - Für wichtige schnelle Korrekturen.
- ```release-*``` - Release-Branches.

### GitLab Flow
GitLab Flow kombiniert das Feature-Driven-Development mit Issue Tracking. Der größte Unterschied zwischen GitLab Flow und GitHub Flow sind sogenannte ```environment```-Branches. Diese helfen bei Anwendungen bei der fertige Feature-Branches nicht immer bereit für direktes Deployment sind. Im wesentlichen basiert GitLab Flow auf den selben Grundlagen wie GitHub Flow, also Feature-Branches, Rebasing, das lokale Testen von commits und Reviews vor Zusammenführung in ```master```. 

### OneFlow
OneFlow fokussiert sich auf nur eine durchgehende Branch. Support-Branches können dennoch verwendet werden, jedoch sollten diese so kurzlebig wie möglich sein. Die Git-History basiert hauptsächlich auf der einen durchgehenden Branch, dadurch basiert eine Version immer auf der vorherigen. Git Tags werden verwendet um gewisse Funktionalitäten der anderen Strategien beizubehalten. OneFlow verwendet keine traditionelle Aufteilung in ```master```, ```development```, etc wie die anderen Strategien.

### Tabellarische Übersicht
| Strategie | Beschreibung | Vorteile | Nachteile |
| --- | --- | --- | --- |
| **Git Flow** | Aufteilung in die permanenten Branches ```master``` und ```develop``` mit zusätzlichen Unterstützungsbranches für die Entwicklung. | + Konsistenter Zustand der Branches zu jedem Zeitpunkt. <br> + Leicht verständlich. <br> + Weitreichende Unterstützung in Git-Tools. | - Git-History schwer nachvollziehbar. <br> - Die Aufteilung der ```master/develop```-Branches kann als redundant angesehen werden. |
| **GitLab Flow** | Feature-driven Development mit Aufteilung in Feature-Branches und Environment-Branches. | + Kontinuierliche Integration und Auslieferung. <br> + Nachvollziehbare Git-History. <br> + Ideal wenn nur eine Version in ```production``` benötigt wird. | - Komplexität höher als bei GitHub Flow. <br> - Schwierigkeiten wenn mehrere Versionen in ```production``` benötigt werden. |
| **OneFlow** | Eine kontinuierliche Branch. Jeder Release basiert auf dem vorherigen. Keine seperate ```develop```-Branch wie Git Flow. | + Flexibel an Team anpassbar. <br> + Vereinfachte Git-History. | - Nicht geeignet wenn technische Kriterien nicht erfüllt werden (Jeder Release basiert auf dem vorherigen) <br> - Nicht geeignet für continuous Deliver/Deployment. | 